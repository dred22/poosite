<?php
spl_autoload_register(function ($class) {
    include "mvc/class/$class.php";
});
$bdd = DataBase::connect();
$model = new Model($bdd);
$controller = new Controller();
$page = $controller->getNewPage($model);
echo $controller->showPage($page, $model);
//var_dump($model->getContent('accueil'));

//echo $page->showPage('<h1>Hello World</h1>');