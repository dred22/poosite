<section class="container">
    <h1>Voilà la Galérie</h1>
 <ul class="row first">
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/1.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/2.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/3.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/4.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/5.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/6.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/7.jpg">
        </figure>
    </li>
    <li class="col-sm-4 col-xs-4">
        <figure>
            <img class="img-responsive"  src="web/img/galerie/8.jpg">
        </figure>
    </li>
</ul>
</section>