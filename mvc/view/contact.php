<form class="form-horizontal container" role="form">
<fieldset>
    <legend>Contact me:</legend>
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" name="email" placeholder="Enter email">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Nom:</label>
        <div class="col-sm-10"> 
          <input type="text" class="form-control" name="nom" placeholder="Nom">
        </div>
      </div>
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
          <input type="hidden" name="idForm" value="contact">
          <button type="submit" class="btn btn-default">Submit</button>
        </div>
      </div>
<fieldset>
</form>
<?php 
  if(isset($message)){
    echo $message;
  } 
?>