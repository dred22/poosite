<?php
class Controller {
    public $pageObj;
    protected $pageRequiered;
    protected $idForm;

    function __construct(){ 
        $this->initialize();
        
    }

    function initialize(){
        $this->page = new Page;
        $this->setCss();
        $this->setScript();
        $this->pageRequiered = false;
        $this->idForm = false;
        if(!empty($_REQUEST['page'])){
            $this->pageRequiered = $_REQUEST['page'];
        }
        if(!empty($_REQUEST['idForm'])){
             
            $this->idForm = $_REQUEST['idForm'];
        }
    }
    
    function setCss(){
        $this->page->setCss(['href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
                            'integrity'=>'sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7',
                            'crossorigin'=>'anonymous'
                            ]);
        $this->page->setCss('web/css/style.css');
    }
    
    function setScript(){
        $this->page->setScript('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js');
        $this->page->setScript(['src'=>'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
                                'integrity'=>'sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS',
                                'crossorigin'=>'anonymous'
                                ]);
        $this->page->setScript('web/js/jquery.bsPhotoGallery.js');
        $this->page->setScript('web/js/script.js');        
    }
    function setMenu($listMenu, $pageActive){
        $this->page->setNavbar($listMenu, $pageActive);
    }
    
    function getNewPage(Model $model){
        //$this->setMenu(['accueil','galerie','contact','newsletter','login']);
        return $this->page;
        
    }
    function showPage(Page $pageObj, Model $model){
        
        if($this->idForm){
            //echo '<pre>';
            $content = $model->getContent($this->idForm);
            //echo $content.'---------'.__FILE__;
            $pageObj->setBody($content);
            $formTreatmentObj = ucfirst($this->idForm);
            $formTreatmentObj = new $formTreatmentObj($pageObj);
            
            //die(var_dump($pageObj));
            $this->setMenu($model->getPagesList(),$this->idForm);
            return $pageObj->showPage();
        }
        
        
        if($this->pageRequiered){
            $content = $model->getContent($this->pageRequiered);
            $this->setMenu($model->getPagesList(),$this->pageRequiered);
        } else {
            $content = $model->getContent('accueil');
            $this->setMenu($model->getPagesList(),'accueil');
        }
        $pageObj->setBody($content);
        return $pageObj->showPage();
        
    }
}