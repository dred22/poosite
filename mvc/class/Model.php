<?php
class Model {
    protected $bdd;
    function __construct(PDO $bdd){
        $this->bdd = $bdd;
    }
    function getPagesList(){
        $query = 'select name from page';
        $request = $this->bdd->query($query);
        $respons = $request->fetchAll(PDO::FETCH_COLUMN);
        //$respons = $request->fetchColumn();
        if(!empty($respons)){
            return $respons;
        }
        return false;
    }
    function getContent($pageName){
        $request = $this->bdd->prepare('select content from page where name = :pageName');
        $request->bindParam(':pageName',$pageName);
        $request->execute();
        //var_dump($query,$this->bdd->errorInfo());
        $respons = $request->fetch(PDO::FETCH_COLUMN);
        //$respons = $request->fetchColumn();
        if(!empty($respons)){
            return $respons;
        }
        return false;
    }
}