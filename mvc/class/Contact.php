<?php
class Contact{
    //protected page;
    function __construct(Page $page){ 
        $this->initialize($page);
    }
    function initialize(Page $page){
        //var_dump($page);
        $content = $page->getBody();
        //echo $content.'---------'.__FILE__;
        if(!empty($_REQUEST['email'])){
            $message =  '<div class="alert alert-success" role="alert">Bonjour '.htmlspecialchars($_REQUEST['nom']).' ! Votre demande a été bien enregistré</div>';
        } else {
            $message = '<div class="alert alert-warning" role="alert">Veuillez bien remplir tous les champs!</div>';
        }
        $page->setBody($content.$message);
    }
}