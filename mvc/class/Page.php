<?php
class Page {
    protected $scriptsList;
    protected $cssList;
    protected $document;
    protected $head;
    protected $body;
    protected $foot;
    protected $navbar;
    
    function __construct(){
        $this->initialize();
    }
    function initialize(){
        $this->scriptsList = [];
        $this->cssList = [];
        $this->document = '';
        $this->head = '';
        $this->foot = '';
        $this->navbar = '';
        $this->body = '';
        
    }
    function setCss($css){
        if(empty($css)){
            return false;
        }
        $this->cssList[] = $css;
    }
    function setScript($script){
        if(empty($script)){
            return false;
        }
        $this->scriptsList[] = $script;  
    }
    function setHead(){
        $this->head = 
        <<<CODESET
        <!DOCTYPE html>
        <html lang="fr">
          <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="author" content="Magomed KADIEV">
            <meta name="description" content="Site vitrinne">
            <title>Site</title>
CODESET;
        foreach($this->cssList as $css){
          if(is_array($css)){
            $additionalParameter ='';
            foreach ($css as $key => $value) {
              $additionalParameter.= " $key=\"".$value.' "';
            }
            $this->head .= '<link rel="stylesheet" '.$additionalParameter.' type="text/css" />'."\n";
          } else {
            $this->head .= '<link rel="stylesheet" href="'.$css.'" type="text/css" />'."\n";
          }
        }
          $this->head .=
            <<<CODESET
            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        </head>
        <body>
CODESET;
        
    }
    function setNavbar($navListe, $pageActive){
      if(empty($navListe) || !is_array($navListe)){
        return false;
      }
      $content = <<<CODESET
          <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Site simple</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
CODESET;
      
      foreach($navListe as $value){
        if($value == $pageActive){
            $content .= '<li class="active"><a href="?page='.$value.'">'.ucfirst($value).'</a></li>';
        } else {
            $content .= '<li><a href="?page='.$value.'">'.ucfirst($value).'</a></li>';
        }
      }
      $content .= '          </ul>
        </div><!--/.nav-collapse -->
        </div>
        </nav>';
      $this->navbar = $content;
    }
    
    function setFoot(){
        foreach($this->scriptsList as $script){
        if(is_array($script)){
          $additionalParameter ='';
          foreach ($script as $key => $value) {
            $additionalParameter.= " $key=\"".$value.' "';
          }
          $this->foot .= '<script '.$additionalParameter.'></script>'."\n";
        } else {
          $this->foot .= '<script src="'.$script.'"></script>'."\n";
        }
      }
      $this->foot .= '</body></html>';
    }
    function getBody(){
      return $this->body;
    }
    function setBody($body){
      $this->body = $body;
    }
    function showPage(){
        $this->setHead();
        $this->setFoot();
        $this->document .= $this->head;
        $this->document .= $this->navbar;
        $this->document .= $this->getBody();
        $this->document .= $this->foot;
        return $this->document;
    }
    
}
