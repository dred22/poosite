<?php
class DataBase {
    public static $dbh;
    public static function connect($host='localhost',$dbname='web', $user='root', $password='') {
        if(self::$dbh){
            return self::$dbh;
        }
        $dsn = "mysql:host=$host;dbname=$dbname;charset=UTF8";
        $dbh = null;
        try {
            //$dbh = new PDO($dsn, $user, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $dbh = new PDO($dsn, $user, $password);
            //$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                        
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
            exit(0);
        }
        self::$dbh = $dbh;
        return $dbh;
    }
}
